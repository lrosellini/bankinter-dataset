package com.pt.dataset.dao;


import java.io.IOException;
import java.util.Properties;

import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.impl.HttpSolrServer;


public class BankinterSolrServer {

	static {
		Properties props = new Properties();
		String hostname = null;
		String collectionName = null;
		hostname = props.getProperty("solr.url");
		collectionName = props.getProperty("solr.collection");
		
		SolrServer server = new HttpSolrServer(hostname);

	}
}
