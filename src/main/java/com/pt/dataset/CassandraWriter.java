package com.pt.dataset;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
//import java.util.ArrayList;
//import java.util.Collection;
import java.util.Date;

import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.common.SolrInputDocument;
import org.firebrandocm.dao.impl.hector.HectorPersistenceFactory;

import com.pt.crawler.solr.support.CloudSolrServerFactory;
import com.pt.dataset.iterator.EntityDataGenerator;
import com.pt.dataset.model.CuentaEntity;
import com.pt.dataset.model.Entity;

public class CassandraWriter {
	private HectorPersistenceFactory factory;
	
	
	
	private CuentaEntity currCuenta = null;
	
	//Antonio 25-5-2013
	CloudSolrServerFactory factory_solr = new CloudSolrServerFactory();
	SolrServer solrServer = factory_solr.getSolrServer();
	
	public CassandraWriter() {
		super();
	
		/*
		List<Class<?>> entities = new ArrayList<Class<?>>();
		entities.add(Entity.class);
		entities.add(CounterEntity.class);
		entities.add(CuentaEntity.class);

		try {
			Properties props = new Properties();
			props.load(getClass().getClassLoader().getResourceAsStream("cassandra.properties"));
			clusterName = props.getProperty("cassandra.clustername");
			
			factory = new HectorPersistenceFactory.Builder().
					defaultConsistencyLevel(ConsistencyLevel.valueOf(props.getProperty("cassandra.consistencylevel"))).
					clusterName(clusterName).
					defaultKeySpace(props.getProperty("cassandra.keyspace")).
					contactNodes(new String[] { props.getProperty("cassandra.rpc.addresses") }).
					thriftPort( Integer.parseInt(props.getProperty("cassandra.thrift.port") ) ).
					autoDiscoverHosts(Boolean.parseBoolean(props.getProperty("cassandra.autoDiscoverHosts") )).
					entities(entities).
					build();
		} catch (Exception e) {
			e.printStackTrace();
		}
		*/
	}
	
	public void writeCuenta(Object[] args){
		
		//String cuenta = (String) args[1];
		String nifEmisor = (String)args[2];
		String descEmisor = (String)args[3];
		
		currCuenta = new CuentaEntity(nifEmisor, descEmisor);
		
		
		/*
		
		List<Entity> entities = ce.getAssociatedEntities();
		
		final int numEntities = 40;
		
		for (int i = 0; i < numEntities; i++) {
			Entity e = new Entity(cuenta+"_"+i);
			entities.add(e);
		}
		*/
		
		//factory.persist(ce);
	}

	public void writeColumns(Object[] args) throws SolrServerException, IOException{
		if (currCuenta == null){
			throw new IllegalArgumentException("currCuenta cannot be null");
		}
		
		Entity e = null;
		SolrInputDocument e1 = new SolrInputDocument();
		try {
			e = convertArgs(args);
			e1= convertArgsSolr(args);
		} catch (ParseException ex) {
			ex.printStackTrace();
			
			throw new RuntimeException(ex);
		}
		
		//currCuenta.getAssociatedEntities().add(e);
		currCuenta.getAssociatedEntitiesSolr().add(e1);
		if (currCuenta.getAssociatedEntitiesSolr().size() == EntityDataGenerator.publicCounter ){
		
			//modificar para escribir en solr
			//Collection<SolrInputDocument> docs = currCuenta.getAssociatedEntitiesSolr();
			solrServer.add(currCuenta.getAssociatedEntitiesSolr());
			//solrServer.add(docs);
			solrServer.commit();
			
			
		}
		
		
	}
	
	
	
	@Override
	protected void finalize() throws Throwable {
		System.out.println("Destorying PersistenceFactory");
		factory.destroy();
	}

	private Entity convertArgs(Object[] args) throws ParseException{
		if (args == null){
			return null;
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		final int BASE_INDEX = 1;
		String cuenta = (String) args[BASE_INDEX];
		String id = cuenta +"_"+((Long)args[0]).toString() ;
		Double saldo = (Double) args[BASE_INDEX+1];
		Date fechaContable = sdf.parse((String) args[BASE_INDEX+2]);
		String codigoConcepto = (String)args[BASE_INDEX+3];
		Integer numeroMovimiento = (Integer)args[BASE_INDEX+4];
		Double importe = (Double)args[BASE_INDEX+5];
		String indicaDebeHaber = (String)args[BASE_INDEX+6];
		Integer codInterfase = (Integer)args[BASE_INDEX+7];
		String descripcion = (String)args[BASE_INDEX+8];
		String nifEmisor = (String)args[BASE_INDEX+9];
		String descEmisor = (String)args[BASE_INDEX+10];
		String domiciliado = (String)args[BASE_INDEX+11];
		
		return new Entity(id, cuenta, saldo, fechaContable, codigoConcepto, numeroMovimiento, 
				importe, indicaDebeHaber, codInterfase, descripcion, nifEmisor,
				descEmisor, domiciliado);
		
	}
	
	private SolrInputDocument convertArgsSolr(Object[] args) throws ParseException{
		if (args == null){
			return null;
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SolrInputDocument doc1=new SolrInputDocument();
		final int BASE_INDEX = 1;
		String cuenta = (String) args[BASE_INDEX];
		//String id = cuenta +"_"+((Long)args[0]).toString() ;
		//Double saldo = (Double) args[BASE_INDEX+1];
		//Date fechaContable = sdf.parse((String) args[BASE_INDEX+2]);
		//String codigoConcepto = (String)args[BASE_INDEX+3];
		//Integer numeroMovimiento = (Integer)args[BASE_INDEX+4];
		//Double importe = (Double)args[BASE_INDEX+5];
		//String indicaDebeHaber = (String)args[BASE_INDEX+6];
		//Integer codInterfase = (Integer)args[BASE_INDEX+7];
		//String descripcion = (String)args[BASE_INDEX+8];
		//String nifEmisor = (String)args[BASE_INDEX+9];
		//String descEmisor = (String)args[BASE_INDEX+10];
		//String domiciliado = (String)args[BASE_INDEX+11];
		doc1.addField("id", cuenta +"_"+((Long)args[0]).toString());
		doc1.addField("cuenta", cuenta);
		doc1.addField("saldo",(Double) args[BASE_INDEX+1] );
		doc1.addField("fechaContable", sdf.parse((String) args[BASE_INDEX+2]));
		doc1.addField("codigoConcepto", (String)args[BASE_INDEX+3]);
		doc1.addField("numeroMovimiento", (Integer)args[BASE_INDEX+4]);
		doc1.addField("importe", (Double)args[BASE_INDEX+5]);
		doc1.addField("indicaDebeHaber",(String)args[BASE_INDEX+6] );
		doc1.addField("codInterfase",(Integer)args[BASE_INDEX+7] );
		doc1.addField("descripcion",(String)args[BASE_INDEX+8] );
		doc1.addField("nifEmisor", (String)args[BASE_INDEX+9]);
		doc1.addField("descEmisor", (String)args[BASE_INDEX+10]);
		doc1.addField("domiciliado",(String)args[BASE_INDEX+11] );
		return doc1;
		/*return new SolrInputDocument.addField("id", id);
				(id, cuenta, saldo, fechaContable, codigoConcepto, numeroMovimiento, 
				importe, indicaDebeHaber, codInterfase, descripcion, nifEmisor,
				descEmisor, domiciliado);*/
		
	}
}
