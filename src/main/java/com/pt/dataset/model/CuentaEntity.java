package com.pt.dataset.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.solr.common.SolrInputDocument;
import org.firebrandocm.dao.annotations.ColumnFamily;

@ColumnFamily
public class CuentaEntity {
	
	private String nif; 
	private String descEmisor;

	
	private List<Entity> associatedEntities = new ArrayList<Entity>();
	private List<SolrInputDocument> associatedEntitiesSolr = new ArrayList<SolrInputDocument>();
	
	public List<SolrInputDocument> getAssociatedEntitiesSolr() {
		return associatedEntitiesSolr;
	}
	/*public void setAssociatedEntitiesSolr(
			List<SolrInputDocument> associatedEntitiesSolr) {
		this.associatedEntitiesSolr = associatedEntitiesSolr;
	}*/
	public CuentaEntity(String nif, String descEmisor) {
		super();
		this.nif = nif;
		this.descEmisor = descEmisor;
	}
	public List<Entity> getAssociatedEntities() {
		return associatedEntities;
	}
	public CuentaEntity() {
		super();
	}
	public String getNif() {
		return nif;
	}
	public void setNif(String nif) {
		this.nif = nif;
	}
	public String getDescEmisor() {
		return descEmisor;
	}
	public void setDescEmisor(String descEmisor) {
		this.descEmisor = descEmisor;
	}
	
}
