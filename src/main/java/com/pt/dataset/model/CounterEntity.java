package com.pt.dataset.model;

import org.apache.cassandra.db.marshal.CounterColumnType;
import org.firebrandocm.dao.annotations.Column;
import org.firebrandocm.dao.annotations.ColumnFamily;
import org.firebrandocm.dao.annotations.CounterIncrease;
import org.firebrandocm.dao.annotations.Key;

@ColumnFamily(defaultValidationClass = CounterColumnType.class)
public class CounterEntity {

	@Key
    private String key;

    @Column(counter = true, validationClass = CounterColumnType.class)
    private long counterProperty;

    @CounterIncrease("counterProperty")
    private long counterPropertyIncreaseBy;
    
    
	public CounterEntity(String key, long counterPropertyIncreaseBy) {
		super();
		this.key = key;
		this.counterPropertyIncreaseBy = counterPropertyIncreaseBy;
	}

	public CounterEntity() {
		super();
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public long getCounterProperty() {
		return counterProperty;
	}

	public void setCounterProperty(long counterProperty) {
		this.counterProperty = counterProperty;
	}

	public long getCounterPropertyIncreaseBy() {
		return counterPropertyIncreaseBy;
	}

	public void setCounterPropertyIncreaseBy(long counterPropertyIncreaseBy) {
		this.counterPropertyIncreaseBy = counterPropertyIncreaseBy;
	}
    
    
}
