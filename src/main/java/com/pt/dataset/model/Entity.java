package com.pt.dataset.model;

import java.io.Serializable;
import java.util.Date;

import org.apache.cassandra.db.marshal.DateType;
import org.apache.cassandra.db.marshal.DoubleType;
import org.apache.cassandra.db.marshal.IntegerType;
import org.apache.cassandra.db.marshal.UTF8Type;
import org.firebrandocm.dao.annotations.Column;
import org.firebrandocm.dao.annotations.ColumnFamily;
import org.firebrandocm.dao.annotations.Key;

@ColumnFamily(defaultKeyValidationClass = UTF8Type.class)
public class Entity implements Serializable {
	private static final long serialVersionUID = 715640836269583208L;

	@Key
	private String id;

	@Column(indexed = true)
	private String cuenta;

	@Column(validationClass = DoubleType.class)
	private Double saldo;

	@Column(validationClass = DateType.class)
	private Date fechaContable;
	
	@Column
	private String codigoConcepto;
	
	@Column(validationClass = IntegerType.class)
	private Integer numeroMovimiento;
	
	@Column(validationClass = DoubleType.class)
	private Double importe;
	
	@Column
	private String indicaDebeHaber;
	
	@Column(validationClass = IntegerType.class)
	private Integer codInterfase;
	
	@Column
	private String descripcion;
	
	@Column
	private String nifEmisor;
	
	@Column
	private String descEmisor;
	
	@Column
	private String domiciliado;

	public Entity() {
		super();
	}

	public Entity(String id) {
		super();
		this.id = id;
	}

	public Entity(String id, String cuenta, Double saldo, Date fechaContable,
			String codigoConcepto, Integer numeroMovimiento, Double importe,
			String indicaDebeHaber, Integer codInterfase, String descripcion,
			String nifEmisor, String descEmisor, String domiciliado) {
		super();
		this.id = id;
		this.cuenta = cuenta;
		this.saldo = saldo;
		this.fechaContable = fechaContable;
		this.codigoConcepto = codigoConcepto;
		this.numeroMovimiento = numeroMovimiento;
		this.importe = importe;
		this.indicaDebeHaber = indicaDebeHaber;
		this.codInterfase = codInterfase;
		this.descripcion = descripcion;
		this.nifEmisor = nifEmisor;
		this.descEmisor = descEmisor;
		this.domiciliado = domiciliado;
	}

	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}

	public void setNumeroMovimiento(Integer numeroMovimiento) {
		this.numeroMovimiento = numeroMovimiento;
	}

	public void setImporte(Double importe) {
		this.importe = importe;
	}

	public void setCodInterfase(Integer codInterfase) {
		this.codInterfase = codInterfase;
	}

	public String getCuenta() {
		return cuenta;
	}

	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public Date getFechaContable() {
		return fechaContable;
	}

	public void setFechaContable(Date fechaContable) {
		this.fechaContable = fechaContable;
	}

	public String getCodigoConcepto() {
		return codigoConcepto;
	}

	public void setCodigoConcepto(String codigoConcepto) {
		this.codigoConcepto = codigoConcepto;
	}

	public int getNumeroMovimiento() {
		return numeroMovimiento;
	}

	public void setNumeroMovimiento(int numeroMovimiento) {
		this.numeroMovimiento = numeroMovimiento;
	}

	public double getImporte() {
		return importe;
	}

	public void setImporte(double importe) {
		this.importe = importe;
	}

	public String getIndicaDebeHaber() {
		return indicaDebeHaber;
	}

	public void setIndicaDebeHaber(String indicaDebeHaber) {
		this.indicaDebeHaber = indicaDebeHaber;
	}

	public int getCodInterfase() {
		return codInterfase;
	}

	public void setCodInterfase(int codInterfase) {
		this.codInterfase = codInterfase;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getNifEmisor() {
		return nifEmisor;
	}

	public void setNifEmisor(String nifEmisor) {
		this.nifEmisor = nifEmisor;
	}

	public String getDescEmisor() {
		return descEmisor;
	}

	public void setDescEmisor(String descEmisor) {
		this.descEmisor = descEmisor;
	}

	public String getDomiciliado() {
		return domiciliado;
	}

	public void setDomiciliado(String domiciliado) {
		this.domiciliado = domiciliado;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
